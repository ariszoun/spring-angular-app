# Training Project: Architecture Design for Spring Full-Stack Applications

This project demonstrates architectural design decisions for various use cases in a series of Spring full-stack applications.

## Use Cases

### Case 1: Country and Language Information
- **Functionality**: Users can view a list of countries. Selecting a country from the list will display the languages spoken in that country.

### Case 2: Country Statistics
- **Functionality**: Users can access a list displaying all countries and their statistics. The record shown for each country is the maximum GDP per population ratio over the years.

### Case 3: Comprehensive Statistics Table
- **Functionality**: Users can view a large statistics table including:
  - Columns from the `continents` table
  - `name` from the `regions` table
  - `name` from the `country` table
  - `year`, `population`, and `GDP` from the `country stats` table
- **Additional Features**:
  - Ability to search and filter rows using a dropdown selection of country regions.
  - "date-from" and "date-to" filters for the year of statistics.
  - Pagination for navigating the data.