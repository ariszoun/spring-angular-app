import { Component } from '@angular/core';
import {RouterLink, RouterModule} from "@angular/router";
import {CountriesComponent} from "./countries/countries.component";
import {NgIf} from "@angular/common";

@Component({
  selector: 'app-root',
  standalone: true,
  templateUrl: './app.component.html',
  imports: [
    RouterModule,
    RouterLink,
    CountriesComponent,
    NgIf
  ],
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'client';
  showCountries = false;
}
