import {RouterModule, Routes} from '@angular/router';
import {CountriesComponent} from "./countries/countries.component";
import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {HttpClientModule} from "@angular/common/http";
import {LanguagesComponent} from "./languages/languages.component";
import {CountriesStatsComponent} from "./countries-stats/countries-stats.component";
import {GlobalStatsComponent} from "./global-stats/global-stats.component";
import {FormsModule} from "@angular/forms";

export const routes: Routes = [
  {path: 'countries', component: CountriesComponent},
  {path: 'languages/:countryCode2', component: LanguagesComponent},
  { path: 'countries-stats', component: CountriesStatsComponent },
  {path: 'global-stats', component: GlobalStatsComponent},
];

@NgModule({
  declarations: [

  ],
  imports: [
    FormsModule,
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: []
})
export class AppModule { }
