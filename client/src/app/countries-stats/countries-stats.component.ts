import {Component, OnInit} from '@angular/core';
import {CountryStatsCode3DTO} from "../models/countryStatsCode3.dto";
import {CountryService} from "../services/country.service";
import {NgForOf} from "@angular/common";

@Component({
  selector: 'app-countries-stats',
  standalone: true,
  imports: [
    NgForOf
  ],
  templateUrl: './countries-stats.component.html',
  styleUrl: './countries-stats.component.css'
})
export class CountriesStatsComponent implements OnInit {
  countryStats: CountryStatsCode3DTO[] = [];

  constructor(private countryService: CountryService) { }

  ngOnInit(): void {
    this.fetchCountryStats();
  }

  fetchCountryStats(): void {
    this.countryService.getCountryStatsWithCode3().subscribe({
      next: (stats) => this.countryStats = stats,
      error: (err) => console.error('Failed to fetch country stats', err)
    });
  }
}
