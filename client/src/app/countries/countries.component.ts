import {Component, OnInit} from '@angular/core';
import {CountryDTO} from "../models/country.dto";
import {CountryService} from "../services/country.service";
import {NgForOf} from "@angular/common";
import {Router, RouterModule} from "@angular/router";

@Component({
  selector: 'app-countries',
  standalone: true,
  templateUrl: './countries.component.html',
  styleUrl: './countries.component.css',
  imports: [RouterModule, NgForOf]
})
export class CountriesComponent implements OnInit{
  countries: Array<CountryDTO> = [];

  constructor(private countryService: CountryService, private router: Router) {}

  ngOnInit(): void {
    this.fetchCountries();
  }

  fetchCountries(): void {
    this.countryService.getCountries().subscribe({
      next: (countries) => this.countries = countries,
      error: (err) => console.error('Failed to fetch countries', err)
    });
  }

  onCountrySelect(countryCode2: string): void {
    console.log('Navigating with countryCode2:', countryCode2);
    this.router.navigate(['/languages', countryCode2])
      .then(success => {
        if (!success) {
          console.error('Navigation failed!');
        } else {
          console.log('Navigation succeeded');
        }
      })
      .catch(error => console.error('Navigation error:', error));
  }
}
