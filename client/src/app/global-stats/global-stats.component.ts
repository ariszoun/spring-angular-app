import { Component, OnInit } from '@angular/core';
import {GlobalStatsDTO} from "../models/global-stats.dto";
import {GlobalStatsService} from "../services/global-stats.service";
import {NgForOf} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {RegionService} from "../services/region-service";
import {Region} from "../models/region.dto";


@Component({
  selector: 'app-global-stats',
  standalone: true,
  templateUrl: './global-stats.component.html',
  imports: [
    FormsModule,
    NgForOf
  ],
  styleUrls: ['./global-stats.component.css']
})
export class GlobalStatsComponent implements OnInit {
  globalStats: GlobalStatsDTO[] = [];
  regions: Region[] = [];
  regionId?: number;
  startYear?: number;
  endYear?: number;
  page: number = 1;
  size: number | 'All' = 20;

  constructor(
    private globalStatsService: GlobalStatsService,
    private regionService: RegionService
    ) {}

  ngOnInit(): void {
    this.fetchGlobalStats();
    this.fetchRegions();
  }

  fetchRegions(): void {
    this.regionService.getRegions().subscribe(
      data => this.regions = data,
      error => console.error('Error fetching regions', error)
    );
  }

  fetchGlobalStats(): void {
    this.globalStatsService.getFilteredStats(this.regionId, this.startYear, this.endYear, this.size !== 'All' ? this.page : undefined, this.size)
      .subscribe(stats => {
        this.globalStats = stats;
      });
  }

  onFilterApply(): void {
    this.globalStatsService.getFilteredStats(this.regionId, this.startYear, this.endYear).subscribe(
      stats => this.globalStats = stats,
      error => console.error('Failed to fetch filtered stats', error)
    );
  }
    onPageSizeChange(newSize: number | 'All'): void {
        this.size = newSize;
        this.page = 1;
        this.fetchGlobalStats();
    }

    onNextPage(): void {
        if (this.size !== 'All') {
            this.page++;
            this.fetchGlobalStats();
        }
    }

    onPreviousPage(): void {
        if (this.page > 1 && this.size !== 'All') {
            this.page--;
            this.fetchGlobalStats();
        }
  }

}
