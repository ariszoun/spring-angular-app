import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {LanguageDTO} from "../models/language.dto";
import {CountryService} from "../services/country.service";
import {NgForOf} from "@angular/common";

@Component({
  selector: 'app-languages',
  standalone: true,
  // imports: [ActivatedRoute],
  templateUrl: './languages.component.html',
  imports: [
    NgForOf
  ],
  styleUrl: './languages.component.css'
})
export class LanguagesComponent implements OnInit {
  languages: LanguageDTO[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private countryService: CountryService
  ) {}

  ngOnInit(): void {
    const countryCode2 = this.route.snapshot.paramMap.get('countryCode2');
    if (countryCode2) {
      this.fetchLanguages(countryCode2);
    }
  }

  fetchLanguages(countryCode2: string): void {
    this.countryService.getLanguagesByCountryCode(countryCode2).subscribe({
      next: (languages) => this.languages = languages,
      error: (err) => console.error('Failed to fetch languages', err)
    });
  }
}
