export interface CountryDTO {
  name: string;
  area: number;
  countryCode2: string;
}
