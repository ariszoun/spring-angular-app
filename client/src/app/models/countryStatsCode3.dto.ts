export interface CountryStatsCode3DTO {
  name: string;
  countryCode3: string;
  year: number;
  population: number;
  gdp: number;
}
