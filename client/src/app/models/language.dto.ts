export interface LanguageDTO {
  languageId: number;
  language: string;
}
