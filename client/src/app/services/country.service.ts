import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {CountryDTO} from "../models/country.dto";
import {LanguageDTO} from "../models/language.dto";
import {CountryStatsCode3DTO} from "../models/countryStatsCode3.dto";

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  private baseUrl: string = 'http://localhost:8080/rest/v1';

  constructor(private http: HttpClient) {}

  getCountries(): Observable<CountryDTO[]> {
    return this.http.get<CountryDTO[]>(`${this.baseUrl}/countries/name-area-code2`);
  }

  getLanguagesByCountryCode(countryCode2: string): Observable<LanguageDTO[]> {
    return this.http.get<LanguageDTO[]>(`${this.baseUrl}/languages/${countryCode2}/languages`);
  }

  getCountryStatsWithCode3(): Observable<CountryStatsCode3DTO[]> {
    return this.http.get<CountryStatsCode3DTO[]>(`${this.baseUrl}/countries/stats-code3`);
  }
}
