import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GlobalStatsDTO } from '../models/global-stats.dto';

@Injectable({
  providedIn: 'root'
})
export class GlobalStatsService {
  private baseUrl: string = 'http://localhost:8080/rest/v1';

  constructor(private http: HttpClient) {}

  getFilteredStats(regionId?: number, startYear?: number, endYear?: number, page?: number, size?: number | 'All'): Observable<GlobalStatsDTO[]> {
    let params = new HttpParams();
    if (regionId) params = params.append('regionId', regionId.toString());
    if (startYear) params = params.append('startYear', startYear.toString());
    if (endYear) params = params.append('endYear', endYear.toString());

    if (size !== 'All' && page !== undefined && size !== undefined) {
      params = params.append('page', page.toString() ?? '1');
      params = params.append('size', size.toString() ?? '20');
    }

    return this.http.get<GlobalStatsDTO[]>(`${this.baseUrl}/global/filtered-stats`, { params });
  }
}
