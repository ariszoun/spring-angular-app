import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Region} from "../models/region.dto";

@Injectable({
  providedIn: 'root'
})
export class RegionService {
  private baseUrl: string = 'http://localhost:8080/rest/v1';

  constructor(private http: HttpClient) {}

  getRegions(): Observable<Region[]> {
    return this.http.get<Region[]>(`${this.baseUrl}/regions`);
  }
}
