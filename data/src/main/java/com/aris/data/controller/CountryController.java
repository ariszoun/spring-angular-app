package com.aris.data.controller;

import com.aris.data.dto.CountryDTOnameAreaCode2;
import com.aris.data.dto.CountryStatsCode3DTO;
import com.aris.data.mapper.CountryMapper;
import com.aris.data.model.Country;
import com.aris.data.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/countries")
public class CountryController {

    @Autowired
    private CountryService countryService;

    @GetMapping
    public ResponseEntity<List<Country>> getAllCountries() {
        List<Country> countries = countryService.findAllCountries();
        return new ResponseEntity<>(countries, HttpStatus.OK);
    }

    @GetMapping("/name-area-code2")
    public ResponseEntity<List<CountryDTOnameAreaCode2>> getAllCountriesOrderedByName() {
        List<CountryDTOnameAreaCode2> countries = countryService.findCountriesWithNameAreaCode2OrderedByNameDTO();
        return new ResponseEntity<>(countries, HttpStatus.OK);
    }

    @GetMapping("/stats-code3")
    public ResponseEntity<List<CountryStatsCode3DTO>> getCountryStatsWithCode3() {
        List<CountryStatsCode3DTO> countryStats = countryService.findCountryStatsWithCode3();
        return new ResponseEntity<>(countryStats, HttpStatus.OK);
    }
}
