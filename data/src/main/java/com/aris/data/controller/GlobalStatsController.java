package com.aris.data.controller;

import com.aris.data.dto.GlobalStatsDTO;
import com.aris.data.service.CountryService;
import com.aris.data.service.GlobalStatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/global")
public class GlobalStatsController {

    @Autowired
    private GlobalStatsService globalStatsService;

    @GetMapping("/filtered-stats")
    public ResponseEntity<List<GlobalStatsDTO>> getFilteredStats(
            @RequestParam(required = false) Integer regionId,
            @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy") Integer startYear,
            @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy") Integer endYear,
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer size) {
        List<GlobalStatsDTO> stats = globalStatsService.findAllWithFilters(regionId, startYear, endYear, page, size);
        return new ResponseEntity<>(stats, HttpStatus.OK);
    }

}
