package com.aris.data.controller;


import com.aris.data.dto.LanguageDTO;
import com.aris.data.service.LanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/languages")
public class LanguageController {

    private final LanguageService languageService;

    @Autowired
    public LanguageController(LanguageService languageService) {
        this.languageService = languageService;
    }

    @GetMapping("/{countryCode2}/languages")
    public ResponseEntity<List<LanguageDTO>> getLanguagesByCountryCode2(@PathVariable String countryCode2) {
        List<LanguageDTO> languages = languageService.findLanguagesByCountryCode2(countryCode2);
        if (languages.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(languages, HttpStatus.OK);
    }
}
