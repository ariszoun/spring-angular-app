package com.aris.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CountryDTOnameAreaCode2 {
    private String name;
    private Double area;
    private String countryCode2;
}
