package com.aris.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CountryStatsCode3DTO {
    private String name;
    private String countryCode3;
    private Integer year;
    private Integer population;
    private Double gdp;
}
