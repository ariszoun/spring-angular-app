package com.aris.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GlobalStatsDTO {
    private String continentName;
    private String regionName;
    private String countryName;
    private Integer year;
    private Integer population;
    private Double gdp;
}
