package com.aris.data.mapper;

import com.aris.data.model.Language;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CountryLanguageMapper {
    @Select("SELECT l.* FROM languages l INNER JOIN country_languages cl ON l.language_id = cl.language_id WHERE cl.country_id = #{countryId}")
    @Results(value = @Result(property = "languageId", column = "language_id"))
    List<Language> findLanguagesByCountryId(Integer countryId);
}
