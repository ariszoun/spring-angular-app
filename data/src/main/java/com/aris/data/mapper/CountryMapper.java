package com.aris.data.mapper;

import com.aris.data.dto.CountryStatsCode3DTO;
import com.aris.data.model.Country;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDate;
import java.util.List;

@Mapper
public interface CountryMapper {

    @Select("SELECT country_id, name, area, national_day, country_code2, country_code3, region_id FROM countries ORDER BY name")
    @Results(value = {
            @Result(property = "countryId", column = "country_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "area", column = "area"),
            @Result(property = "nationalDay", column = "national_day", javaType = LocalDate.class),
            @Result(property = "countryCode2", column = "country_code2"),
            @Result(property = "countryCode3", column = "country_code3"),
            @Result(property = "regionId", column = "region_id")
    })
    List<Country> findAllCountries();

    @Select("SELECT name, area, country_code2 FROM countries ORDER BY name")
    @Results(value = {
            @Result(property = "name", column = "name"),
            @Result(property = "area", column = "area"),
            @Result(property = "countryCode2", column = "country_code2")
    })
    List<Country> findCountriesWithNameAreaCode2OrderedByName();

    @Select("SELECT country_id FROM countries WHERE country_code2 = #{countryCode2}")
    @Results(value = @Result(property = "countryId", column = "country_id"))
    Country findCountryIdByCountryCode2(String countryCode2);

    @Select("SELECT c.name, c.country_code3, cs.year, cs.population, cs.gdp " +
            "FROM countries c JOIN country_stats cs ON c.country_id = cs.country_id " +
            "WHERE (cs.gdp / cs.population) = (SELECT MAX(gdp/population)FROM country_stats " +
            "WHERE country_id = c.country_id)")
    @Results(value = {
            @Result(property = "name", column = "name"),
            @Result(property = "countryCode3", column = "country_code3"),
            @Result(property = "year", column = "year"),
            @Result(property = "population", column = "population"),
            @Result(property = "gdp", column = "gdp")
    })
    List<CountryStatsCode3DTO> findCountryStatsWithCode3();

}
