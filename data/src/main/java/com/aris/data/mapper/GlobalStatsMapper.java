package com.aris.data.mapper;

import com.aris.data.dto.GlobalStatsDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface GlobalStatsMapper {

    @Select("<script>" +
            "SELECT cont.name as continentName, reg.name as regionName, cou.name as countryName, " +
            "cs.year, cs.population, cs.gdp " +
            "FROM continents cont " +
            "JOIN regions reg ON cont.continent_id = reg.continent_id " +
            "JOIN countries cou ON reg.region_id = cou.region_id " +
            "JOIN country_stats cs ON cou.country_id = cs.country_id " +
            "<where>" +
            "<if test='regionId != null'> AND reg.region_id = #{regionId}</if>" +
            "<if test='startYear != null'> AND cs.year &gt;= #{startYear}</if>" +
            "<if test='endYear != null'> AND cs.year &lt;= #{endYear}</if>" +
            "</where>" +
            "ORDER BY cont.name, reg.name, cou.name, cs.year DESC " +
            "</script>")
    List<GlobalStatsDTO> findAllWithFilters(@Param("regionId") Integer regionId,
                                            @Param("startYear") Integer startYear,
                                            @Param("endYear") Integer endYear);


    @Select("<script>" +
            "SELECT cont.name as continentName, reg.name as regionName, cou.name as countryName, " +
            "cs.year, cs.population, cs.gdp " +
            "FROM continents cont " +
            "JOIN regions reg ON cont.continent_id = reg.continent_id " +
            "JOIN countries cou ON reg.region_id = cou.region_id " +
            "JOIN country_stats cs ON cou.country_id = cs.country_id " +
            "<where>" +
            "<if test='regionId != null'> AND reg.region_id = #{regionId}</if>" +
            "<if test='startYear != null'> AND cs.year &gt;= #{startYear}</if>" +
            "<if test='endYear != null'> AND cs.year &lt;= #{endYear}</if>" +
            "</where>" +
            "ORDER BY cont.name, reg.name, cou.name, cs.year DESC " +
            "LIMIT #{limit} OFFSET #{offset}" +
            "</script>")
    List<GlobalStatsDTO> findAllWithFiltersAndPagination(@Param("regionId") Integer regionId,
                                            @Param("startYear") Integer startYear,
                                            @Param("endYear") Integer endYear,
                                            @Param("offset") Integer offset,
                                            @Param("limit") Integer limit);

}
