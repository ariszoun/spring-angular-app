package com.aris.data.mapper;

import com.aris.data.model.Region;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDate;
import java.util.List;

@Mapper
public interface RegionMapper {

    @Select("SELECT region_id, name FROM regions")
    @Results(value = {
            @Result(property = "regionId", column = "region_id"),
            @Result(property = "name", column = "name"),
    })
    List<Region> findAllRegions();

    @Select("SELECT name FROM regions")
    List<String> findAllRegionNames();

}
