package com.aris.data.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Country {
    private Integer countryId;
    private String name;
    private Double area;
    private LocalDate nationalDay;
    private String countryCode2;
    private String countryCode3;
    private Integer regionId;
}
