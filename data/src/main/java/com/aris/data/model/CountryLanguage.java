package com.aris.data.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CountryLanguage {
    private Integer countryId;
    private Integer languageId;
    private Boolean official;
}
