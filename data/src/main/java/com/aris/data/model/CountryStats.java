package com.aris.data.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CountryStats {
    private Integer countryId;
    private Integer year;
    private Integer population;
    private Double gdp;
}
