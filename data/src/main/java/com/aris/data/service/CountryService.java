package com.aris.data.service;


import com.aris.data.dto.CountryDTOnameAreaCode2;
import com.aris.data.dto.CountryStatsCode3DTO;
import com.aris.data.mapper.CountryMapper;
import com.aris.data.model.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CountryService {

    private final CountryMapper countryMapper;

    @Autowired
    public CountryService(CountryMapper countryMapper) {
        this.countryMapper = countryMapper;
    }

    public List<Country> findAllCountries() {
        return countryMapper.findAllCountries();
    }

    public List<CountryDTOnameAreaCode2> findCountriesWithNameAreaCode2OrderedByNameDTO() {
        List<Country> countries = countryMapper.findCountriesWithNameAreaCode2OrderedByName();
        return countries.stream()
                .map(country -> new CountryDTOnameAreaCode2(country.getName(), country.getArea(), country.getCountryCode2()))
                .collect(Collectors.toList());
    }

    public List<CountryStatsCode3DTO> findCountryStatsWithCode3() {
        return countryMapper.findCountryStatsWithCode3();
    }
}
