package com.aris.data.service;

import com.aris.data.dto.GlobalStatsDTO;
import com.aris.data.mapper.CountryMapper;
import com.aris.data.mapper.GlobalStatsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GlobalStatsService {

    private final GlobalStatsMapper globalStatsMapper;

    @Autowired
    public GlobalStatsService(GlobalStatsMapper globalStatsMapper) {
        this.globalStatsMapper = globalStatsMapper;
    }

    public List<GlobalStatsDTO> findAllWithFilters(Integer regionId, Integer startYear, Integer endYear, Integer page, Integer size) {
        // If the user select 'All' then tha query params from the client will be different (size will be null) so calling 2 different methods for each case.
        if (size == null) {
           return globalStatsMapper.findAllWithFilters(regionId, startYear, endYear);
        } else {
            Integer offset = (page==null ? 0 : page-1)*size;
            return globalStatsMapper.findAllWithFiltersAndPagination(regionId, startYear, endYear, offset, size);
        }
    }

}
