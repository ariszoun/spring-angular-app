package com.aris.data.service;

import com.aris.data.dto.LanguageDTO;
import com.aris.data.mapper.CountryLanguageMapper;
import com.aris.data.mapper.CountryMapper;
import com.aris.data.model.Country;
import com.aris.data.model.Language;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LanguageService {

    private final CountryLanguageMapper languageMapper;
    private final CountryMapper countryMapper;

    @Autowired
    public LanguageService(CountryLanguageMapper languageMapper, CountryMapper countryMapper) {
        this.languageMapper = languageMapper;
        this.countryMapper = countryMapper;
    }

    public List<LanguageDTO> findLanguagesByCountryId(Integer countryId) {
        List<Language> languages = languageMapper.findLanguagesByCountryId(countryId);
        return languages.stream()
                .map(language -> new LanguageDTO(language.getLanguageId(), language.getLanguage()))
                .collect(Collectors.toList());
    }

    public List<LanguageDTO> findLanguagesByCountryCode2(String countryCode2) {
        Country country = countryMapper.findCountryIdByCountryCode2(countryCode2);
        if (country != null) {
            return languageMapper.findLanguagesByCountryId(country.getCountryId()).stream()
                    .map(language -> new LanguageDTO(language.getLanguageId(), language.getLanguage()))
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }
}
