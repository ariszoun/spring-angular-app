package com.aris.data.service;

import com.aris.data.mapper.RegionMapper;
import com.aris.data.model.Region;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RegionService {

    private final RegionMapper regionMapper;

    @Autowired
    public RegionService(RegionMapper regionMapper) {
        this.regionMapper = regionMapper;
    }

    public List<Region> findAllRegions() {
        return regionMapper.findAllRegions();
    }

    public List<String> findAllRegionNames() {
        return regionMapper.findAllRegionNames();
    }

}
